import React, { Component } from 'react';
import './myProfile.less';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      gender: 'Male',
      description: '',
      haveRead: false
    }
  }

  nameChangeHandler(event) {
    this.setState({
      name: event.target.value,
    })
  }

  genderChangeHandler() {
    this.setState({
      gender: event.target.value,
    })
  }

  descriptionChangeHandler(event) {
    this.setState({
      description: event.target.value,
    })
  }

  formSubmit(event) {
    event.preventDefault();
    if (this.state.haveRead) {
      console.log(this.state);
    }
  }

  haveReadChangeHandler(event) {
    let state = this.state;
    this.setState({
      haveRead: !state.haveRead
    })
  }

  render() {
    return (
      <form onSubmit={this.formSubmit.bind(this)}>
        <label>Name:</label>
        <br/>
        <input type="text" placeholder='Your Name' className='name-input'
               onChange={this.nameChangeHandler.bind(this)} value={this.state.name}/>
        <br/>
        <select value={this.state.gender} onChange={this.genderChangeHandler.bind(this)}>
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </select>
        <br/>
        <textarea onChange={this.descriptionChangeHandler.bind(this)} value={this.state.description}/>
        <br/>
        <input id='haveReade' type='checkbox' value={this.state.haveRead} onChange={this.haveReadChangeHandler.bind(this)}/>
        <label htmlFor='haveReade'>I have eread the terms of conduct</label>
        <br/>
        <input type="submit" value='Submit'/>
      </form>
    );
  }
}

export default MyProfile;


